const error = require('./error');

// eslint-disable-next-line no-unused-vars
module.exports = ([commandName, teamName, name, hp, ap], gameData) => {
  const teamIndex = gameData.teams.findIndex(team => team.name === teamName);
  const monpoke = {
    name,
    hp: Number(hp),
    ap: Number(ap),
    inBattle: false,
  };

  if (monpoke.ap < 1 || monpoke.hp < 1) {
    return error();
  }

  if (teamIndex === -1) {
    const team = {
      name: teamName,
      monpokeList: [monpoke],
    };
    gameData.teams.push(team);
  } else {
    gameData.teams[teamIndex].monpokeList.push(monpoke);
  }

  return {
    gameData,
    result: `${name} has been assigned to team ${teamName}!`,
  };
};
