const attackEnemy = require('./attackEnemy');
const chooseFighter = require('./chooseFighter');
const createMonpokeOnTeam = require('./createMonpokeOnTeam');
const error = require('./error');

module.exports = commandText => {
  const commands = commandText.split('\n').filter(command => command !== '');
  let gameData = {
    teams: [],
    turnTeamId: 0,
  };
  const resultsList = commands.map(command => {
    const commandComponents = command.split(' ');
    const verb = commandComponents[0];
    if (verb === 'CREATE') {
      const create = createMonpokeOnTeam(commandComponents, gameData);
      gameData = create.gameData;
      return create.result;
    }
    if (verb === 'ICHOOSEYOU') {
      const iChooseYou = chooseFighter(commandComponents, gameData);
      gameData = iChooseYou.gameData;
      return iChooseYou.result;
    }
    if (verb === 'ATTACK') {
      const attack = attackEnemy(gameData);
      gameData = attack.gameData;
      return attack.result;
    }
    return error();
  });
  return resultsList.join('\n');
};
